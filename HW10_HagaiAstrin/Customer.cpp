#include "Customer.h"

Customer::Customer(string name)
{
	_name = name;
}

Customer::Customer()
{

}

Customer::~Customer()
{}

double Customer::totalSum() const
{
	double sum = 0;
	for (set<Item>::iterator i = _items.begin(); i != _items.end(); i++)
	{
		Item temp = *i;
		sum += temp.totalPrice();
	}

	return sum;
}

void Customer::addItem(Item item)
{
	if (_items.size() == 0)
	{
		_items.insert(item);
	}
	else
	{
		if (_items.find(item) != _items.end())
		{
			Item temp = *_items.find(item);
			_items.erase(_items.find(item));
			temp.addOneToCount();
			_items.insert(temp);
		}
		else
		{
			_items.insert(item);
		}
	}
}

void Customer::removeItem(Item item)
{
	_items.erase(_items.find(item));
}

string Customer::getName()
{
	return _name;
}

void Customer::setName(string name)
{
	_name = name;
}

void Customer::setItems(set<Item> items)
{
	_items = items;
}

set<Item> Customer::getItems()
{
	return _items;
}

