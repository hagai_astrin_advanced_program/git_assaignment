#include"Customer.h"
#include<map>

// CHANGES FOR V.2 OF THE FILE FOR GIT HW!!!

pair<string, Customer> customer;
map<string, Customer> abcCustomers;
Item itemList[10] = {
	Item("Milk", "00001", 5.3),
	Item("Cookies", "00002", 12.6),
	Item("bread", "00003", 8.9),
	Item("chocolate", "00004", 7.0),
	Item("cheese", "00005", 15.3),
	Item("rice", "00006", 6.2),
	Item("fish", "00008", 31.65),
	Item("chicken", "00007", 25.99),
	Item("cucumber", "00009", 1.21),
	Item("tomato", "00010", 2.32) };

void menu1(); // Choice 1.
void addMenu(Customer c); // Add an item.
void menu2(); // Choice 2.
void menu3(); // Choice 3.
void removeMenu(string name); // Remove an item.

int main()
{
	int userChoice;
	string name;
	Customer temp("default");

	do
	{
		do // Check user choice:
		{
			cout << "Welcome to MagshiMart" << endl;
			cout << "1.		to sign as customer and buy items" << endl;
			cout << "2.		to update existing customer's items" << endl;
			cout << "3.		to print the customer who pays the most" << endl;
			cout << "4.		to exit" << endl;
			cin >> userChoice;
		} while (userChoice != 1 && userChoice != 2 && userChoice != 3 && userChoice != 4);
		if (userChoice != 4) // If user didn't type 4(exit):
		{
			switch (userChoice)
			{
			case 1:
				menu1();
				break;
			case 2:
				menu2();
				break;
			case 3:
				menu3();
				break;
			default:
				break;
			}
		}
	} while (userChoice != 4);
	

	system("PAUSE");
	return 0;
}

void menu1()
{
	int itemChoice;
	string name;
	Customer temp("default");

	cout << "Enter customer name: ";
	cin >> name;
	if (abcCustomers.find(name) != abcCustomers.end())
	{
		cout << "Error: can't sign in as " << name << "\nName already exists!" << endl;
	}
	else // If name doesn't exist, create new customer.
	{
		temp.setName(name);
		do
		{
			cout << "The items you can buy are[between 1-10]: (0 to exit)" << endl;
			cin >> itemChoice;
			if (itemChoice != 0)
			{
				temp.addItem(itemList[itemChoice - 1]);
			}
		} while (itemChoice != 0);

		pair<string, Customer> customer(name, temp);
		abcCustomers.insert(customer);
	}

}

void menu2()
{
	int menuChoice;
	string name;

	cout << "Enter name: ";
	cin >> name;

	if (abcCustomers.find(name) != abcCustomers.end())
	{
		do
		{
			cout << "1.		Add items" << endl;
			cout << "2.		Remove items" << endl;
			cout << "3.		Back to menu" << endl;
			cin >> menuChoice;
		} while (menuChoice != 1 && menuChoice != 2 && menuChoice != 3);

		switch (menuChoice)
		{
		case 1:
			addMenu(abcCustomers.find(name)->second);
			break;
		case 2:
			removeMenu(name);
			break;
		default:
			break;
		}
	}
	else
	{
		cout << "Error: customer doesn't exist" << endl;
	}
}

void removeMenu(string name)
{
	map<string, Customer>::iterator ptr_customer = abcCustomers.find(name); // ptr to index of customer.
	set<Item> itemSet = ptr_customer->second.getItems(); // Items list of customer.
	set<Item>::iterator currItem;
	string itemName;
	bool flag = true;

	do
	{
		cout << name + " item list:" << endl;

		for (currItem = itemSet.begin(); currItem != itemSet.end(); currItem++) // Print all items.
		{
			Item temp = *currItem;
			cout << temp.getItemName() + "  ||  ";
			cout << " Count: ";
			cout << temp.getCount() << endl;
		}
		cout << "------------------------------------" << endl << endl;

		cout << "Enter requested item to be removed (tpye 'exit' to exit): ";
		cin >> itemName;
		
		flag = true;
		currItem = itemSet.begin();
		while (flag && currItem != itemSet.end())
		{
			Item temp = *currItem;
			if (temp.getItemName().compare(itemName) == 0) // Check the iterator
			{
				if (temp.getCount() > 1)
				{
					// Reduce count by 1.
					temp.setCount(temp.getCount() - 1); 
					itemSet.erase(itemSet.find(temp));
					itemSet.insert(temp);
					ptr_customer->second.setItems(itemSet);
					flag = false;
				}
				else
				{
					// Remove item.
					itemSet.erase(itemSet.find(temp));
					ptr_customer->second.setItems(itemSet);
					flag = false;
				}
			}
			else
				currItem++; // Continue in search.
		}


	} while (itemName.compare("exit") != 0);
	cout << "--------------------------------" << endl << endl;
}

void addMenu(Customer c)
{
	int itemChoice;

	do
	{
		cout << "The items you can but are[between 1-10]: (0 to exit)" << endl;
		cin >> itemChoice;
		if (itemChoice != 0)
		{
			c.addItem(itemList[itemChoice - 1]);
		}
	} while (itemChoice != 0);

	pair<string, Customer> customer(c.getName(), c);
	abcCustomers.erase(abcCustomers.find(c.getName()));
	abcCustomers.insert(customer);
}

void menu3()
{
	map<string, Customer>::iterator i = abcCustomers.begin();
	double sum = 0.0;
	string name;

	for (i; i != abcCustomers.end(); i++)
	{
		Customer customer = i->second;
		if (customer.totalSum() > sum)
		{
			sum = customer.totalSum();
			name = customer.getName();
		}
	}

	cout << name + " had paid the most: " << sum << endl;
}