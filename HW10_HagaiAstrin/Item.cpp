#include "Item.h"

Item::Item(string name, string serialNumber, double unitPrice)
{
	_name = name;
	_serialNumber = serialNumber;
	_count = 1;
	_unitPrice = unitPrice;
}

Item::~Item()
{}

double Item::totalPrice() const
{
	return _count * _unitPrice;
}

bool Item::operator<(const Item& other) const
{
	if (_serialNumber.compare(other._serialNumber) < 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::operator>(const Item& other) const
{
	if (_serialNumber.compare(other._serialNumber) > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Item::operator==(const Item& other) const
{
	if (_serialNumber.compare(other._serialNumber) == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

string Item::getItemName()
{
	return _name;
}

void Item::setItemName(string name)
{
	_name = name;
}

string Item::getSerialNumber()
{
	return _serialNumber;
}

void Item::setSerialNumber(string serialNumber)
{
	_serialNumber = serialNumber;
}

int Item::getCount()
{
	return _count;
}

void Item::setCount(int count)
{
	_count = count;
}

void Item::addOneToCount()
{
	_count += 1;
}

double Item::getUnitPrice()
{
	return _unitPrice;
}

void Item::setUnitPrice(double unitPrice)
{
	_unitPrice = unitPrice;
}